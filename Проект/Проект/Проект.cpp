//#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include "Classes.h"

using namespace std;



DataBase db;

int myhash(char* line)
{
	unsigned int h = 0;
    unsigned char* p;
 
    for (p = (unsigned char*) line; *p != '\0'; p++) {
        h = MULT * h + (*p);
    }
    return h % H;
}




//Input
oTitle OneTitle(char* line)
{
	struct oTitle s0;
	char buf[L];
	int i;
	for(i = 0; i < strlen(line); i++)
	{
		if(line[i] != ':')
			buf[i] = line[i];	
		else 
		{
			buf[i] = '\0';
			break;
		}
	}
	s0.title = (char*) malloc(sizeof(char) * strlen(buf) + sizeof(char));
	memcpy(s0.title, buf, strlen(buf) + sizeof(char));
	i++;
	int k;
	for(k = i; i<strlen(line); i++)
		buf[i-k] = line[i];
	buf[i-k] = '\0';
	s0.type = (datatype)atoi(buf);
	return s0;
}

//Input
oTitle* MakeLine(char* line, int N)
{
	oTitle* titles = (oTitle*)malloc(sizeof(oTitle) * N);
	char arr[L];
	for(int i = 0, j = -1, k = 0;i<= strlen(line); i++)
	{
		if(line[i] != ',' && line[i] != '\0')
			arr[i-j-1] = line[i];
		else
		{
			arr[i-j-1] = '\0';
			titles[k] = OneTitle(arr);
			j = i;
			k++;
		}
	}
	return titles;
}

//Input
post*  MakeOnePost(char* arr, oTitle* titles, int NumberOfColomn, int* NumberOfTextPosts, char datatext[], int* data, post* postdata)
{
	if(titles[NumberOfColomn].type == 1)
		(data[NumberOfColomn]) = atoi(arr);
	else if(titles[NumberOfColomn].type == 2)
	{
		(*NumberOfTextPosts)++;
		
		postdata = (post*)realloc(postdata, sizeof(post) * (*NumberOfTextPosts));;
		
		int yu = strlen(datatext);
		int i;

		for(i = yu; i<yu + strlen(arr); i++)
			datatext[i] = arr[i - yu];
		
		datatext[i] = '\0';

		post p;
		p.deleted = 0;
		p.hash = myhash(arr);
		p.length = strlen(arr);
		p.offset = yu;
		postdata[*NumberOfTextPosts - 1] = p;
		data[NumberOfColomn] = *NumberOfTextPosts - 1;
	}
	return postdata;
}

//Input
post* MakeOneLine(char* line, oTitle* titles, int N, char* datatext, int* data, post* postdata, int* n_posts)
{
	char arr[L];
	int l = 0;
	for(int i = 0, j = -1, k = 0; i<= strlen(line); i++)
	{
		if(line[i] != ',' && line[i] != '\0' && line[i] != '\n' && line[i] != '\a')
			arr[i-j-1] = line[i];
		else
		{
			arr[i-j-1] = '\0';
			if(i - j - 1 == 0)
				continue;
			postdata = MakeOnePost(arr, titles, k, &l, datatext, data, postdata);//����� ���������� �����
			j = i;
			k++;
		}
	}
	*n_posts = l;
	return postdata;
}

//This function will return DataBase type
void input(char* filename)
{
	db = DataBase();
	char buff[L];

	FILE *f;
	
	try
	{
		f = fopen(filename, "r");
		fgets(buff, L, f);

		int zap = 0;
		for(int i = 0; i < strlen(buff); i++)
			if(buff[i] == ',')
			{
				zap = i;
				break;
			}	
		if(zap == 0)
			throw new exception;

		char name[L];
		strncpy(name, buff, zap);
		name[zap] = '\0';

		db = DataBase(name);

		char count_s[L];
		for(int i = zap; i<strlen(buff); i++)
			count_s[i-zap]=buff[i+1];
		int N_tables = atoi(count_s);

		for(int i = 0; i < N_tables; i++)
		{
			fgets(buff, L, f);

			int zap = 0;
			for(int j = 0; j < strlen(buff); j++)
				if(buff[j] == ',')
				{
					zap = j;
					break;
				}
			if(zap == 0)
				throw new exception;

			char tname[L];
			strncpy(tname, buff, zap);
			tname[zap] = '\0';

			int zap2 = 0;
			for(int j = zap + 1; j<strlen(buff); j++)
				if(buff[j] == ',')
				{
					zap2 = j;
					break;
				}
			if(zap2 == 0)
				throw new exception;

			char count_s2[L];
			for(int j = zap+1; j<zap2; j++)
				count_s2[j-zap-1] = buff[j];
			count_s2[zap2-zap-1] = '\0';
			int colomns = atoi(count_s2);

			char count_s3[L];
			for(int j = zap2; j<strlen(buff); j++)
				count_s3[j-zap2]=buff[j+1];
			int lines = atoi(count_s3);

			Table t = Table(tname, lines, colomns);

			fgets(buff, L, f);
			
			oTitle* titles = MakeLine(buff, colomns);
			t.SetOtitles(titles);

			for(int j = 0; j<lines; j++)
			{
				char datatext[L];
				datatext[0] = '\0';
				int* data = (int*)malloc(sizeof(int)*colomns);
				post* postdata = (post*)malloc(0);
				fgets(buff, L, f);
				int posts = 0;
				postdata = MakeOneLine(buff, titles, colomns, datatext, data, postdata, &posts);
				t.AddLine(data, postdata, datatext, posts);
				//free(postdata);
			}
			db.AddTable(t);
		}
		fclose(f);
	}
	catch(exception e)
	{
		printf("Error reading DB\n");
	}
}

//Output
void output(char* filename)
{
	char buff[L], buff1[L];

	try
	{
		FILE *f;
		f = fopen(filename, "w");

		memcpy(buff, db.Name(), strlen(db.Name()) + 1);
		strcat(buff, ","); 
		
		itoa(db.Count(), buff1, P);
		
		strcat(buff, buff1);
		strcat(buff, "\n");
		fwrite(buff, sizeof(char), strlen(buff), f);

		for(int i = 0; i<db.Count(); i++)
		{
			memcpy(buff, db.Tables()[i].Name(), strlen(db.Tables()[i].Name()) + 1);
			strcat(buff, ",");
			
			itoa(db.Tables()[i].Coloumns(), buff1, P);
			strcat(buff, buff1);
			strcat(buff, ",");
			itoa(db.Tables()[i].Lines(), buff1, P);
			
			strcat(buff, buff1);
			strcat(buff, "\n");
			fwrite(buff, sizeof(char), strlen(buff), f);

			buff[0] = '\0';
			for(int j = 0; j<db.Tables()[i].Coloumns(); j++)
			{
				strcat(buff, db.Tables()[i].Headers()[j].title);
				strcat(buff, ":");
				int y = db.Tables()[i].Headers()[j].type;
				itoa(y, buff1, P);
					
				strcat(buff, buff1);
				if(j != db.Tables()[i].Coloumns() - 1)
					strcat(buff, ",");
				else strcat(buff, "\n");
			}
			fwrite(buff, sizeof(char), strlen(buff), f);

			int j;
			for(j = 0; j<db.Tables()[i].Lines(); j++)
			{
				buff[0] = '\0';
				for(int k = 0; k<db.Tables()[i].Coloumns(); k++)
				{
					if(db.Tables()[i].Headers()[k].type == 1)
						itoa(db.Tables()[i].Maintable()[j][k], buff1, P);
					else 	
					{
						int index = db.Tables()[i].Maintable()[j][k];
						post p = db.Tables()[i].Posts()[index];
						memcpy(buff1, db.Tables()[i].TextData() + p.offset, p.length);
						buff1[p.length]='\0';
					}
					strcat(buff, buff1);
					if(k != db.Tables()[i].Coloumns() - 1)
						strcat(buff, ",");
				}
				if(!(i == db.Count() - 1 && j == db.Tables()[i].Lines() - 1))
					strcat(buff, "\n");
				fwrite(buff, sizeof(char), strlen(buff), f);
			}
		}

		fclose(f);
	}
	catch(exception e)
	{
		printf("Error writing\n");
	}

}

//� name �������� ������ ����� ����� �������, � line �������� ��� ���������
//������� ������ - line
//���� ���������� ����� ��� �������, �� � line � name ���� � �� ��
void GetTwoLines(char* line, char* name, char symbol)
{
	char *k = strchr(line, symbol);
	int probel;
	if(k != 0)
		probel = k-line;
	else probel = strlen(line);

	memcpy(name, line, probel);
	name[probel] = '\0';

	char line2[L];

	memcpy(line, line + strlen(name) + 1, strlen(line) - strlen(name));
}
 
void GetCondition(char* line, Condition* c)
{
	char* eq = strchr(line, '=');
	char* more = strchr(line, '>');
	char* less = strchr(line, '<');
	char* f = 0x00, f1 = 0x00;
	int l = 0;

	if(eq != 0x00)
	{
		if(more == 0x00 && less == 0x00)
		{
			char* neq = strchr(line, '!');
			if(neq != 0x00)
			{
				c->ct = (condtype)2;
				f = neq;
				l = 2;
			}
			else 
			{
				c->ct = (condtype)1;
				f = eq;
				l = 1;
			}
		}
		else 
			if(more == eq - sizeof(char))
			{
				c->ct = (condtype)4;
				f = more;
				l = 2;
			}
			else
				if(less == eq - sizeof(char))
				{
					c->ct = (condtype)6;
					f = less;
					l = 2;
				}
	}
	else 
		if(more == 0x00)
		{
			c->ct = (condtype)5;
			f = less;
			l = 1;
		}
		else 
			if(less == 0x00)
			{
				c->ct = (condtype)3;
				f = more;
				l = 1;
			}
	if(c->ct == 0) throw new exception;


	memcpy(c->title, line, f-line);
	c->title[f-line] = '\0';

	if(strchr(f, '\'') == 0x00 )
	{
		memcpy(c->data, f+l, strlen(f) - l + 1);
		c->dt = (datatype)1;
	}
	else
	{
		memcpy(c->data, f + l + 1, strlen(f) - l - 2);
		c->data[strlen(f) - l - 2] = '\0';
		c->dt = (datatype)2;
	}

	if(c->ct == (condtype)0 || c->dt == (datatype)0)
		throw new exception;
}

void GetVivTitles(char* line, int N, char** arr)
{
	char* zap = line;
	char* zap1 = strchr(line, ',');

	for(int i = 0; i < N; i++)
	{
		if(zap1 == 0x00)
			zap1 = zap + strlen(zap);
		memcpy(arr[i], zap, zap1-zap);
		arr[i][zap1 - zap] = '\0';
		zap = zap1 + 1;
		zap1 = strchr(zap, ',');
	}
}

int Operation1(condtype c, char* line1, char* line2, datatype dt)
{
	int s;
	if(dt == (datatype)1)
	{
		int a = atoi(line1);
		int b = atoi(line2);
		s = a - b;
	}
	else s = strcmp(line1, line2);
	if((c == 1 || c == 6 || c == 4) && s == 0)
		return 1;
	if((c== 3 || c == 4) && s > 0)
		return 1;
	if((c == 5 || c == 6) && s < 0)
		return 1;
	if(c == 2 && s != 0)
		return 1;
	return 0;
}

void ShowSelected(int y, int N, sel_tab* tab, char** titles, int n_titles, Condition c, sel_sort sort)
{
	/*char*** mainarr;*/
	int** mainarr_off;
	int** mainarr_l;
	char* buf = (char*)malloc(sizeof(char) * L);
	char* textline = (char*)malloc(sizeof(char) * LS);
	textline[0] = '\0';

	int all  = 0;
	int a_lines, a_coloumns;

	if(titles[0][0] == '*')
		all  = 1;

	if(N == 2)
	{
		int* a = (int*)malloc(sizeof(int) * N); //������� ������
		int* b = (int*)malloc(sizeof(int) * N); //������ �������� �� ������� ����������
		for(int i = 0; i < db.Count(); i++)
			for(int j = 0; j < N; j++)
				if(strcmp(tab[j].tablename, db.Tables()[i].Name()) == 0)
					a[j] = i;
				
		if(n_titles == -1)
			n_titles = db.Tables()[a[0]].Coloumns() + db.Tables()[a[1]].Coloumns();
		
		int ind1 = -1, ind2 = -1; //���� �� ������� �� ������� ����������

		for(int i = 0; i < db.Tables()[a[0]].Coloumns(); i++)
			if(strcmp (db.Tables()[a[0]].Headers()[i].title, tab[0].title) == 0)
			{
				ind1 = i;
				break;
			}
		for(int i = 0; i < db.Tables()[a[1]].Coloumns(); i++)
			if(strcmp (db.Tables()[a[1]].Headers()[i].title, tab[1].title) == 0)
			{
				ind2 = i;
				break;
			}

		if(db.Tables()[a[0]].Headers()[ind1].type != db.Tables()[a[1]].Headers()[ind2].type)
			cout<<"error uniting"<<endl;
		int type = db.Tables()[a[0]].Headers()[ind1].type;
		
		//������ ������� �����������
		int** unite_table = (int**)malloc(sizeof(int*) * db.Tables()[a[0]].Lines());
		int s = 0;

		for(int i = 0; i < db.Tables()[a[0]].Lines(); i++)
			unite_table[i] = (int*)malloc(db.Tables()[a[1]].Lines() * sizeof(int));

		for(int i = 0; i < db.Tables()[a[0]].Lines(); i++)
		{
			for(int j = 0; j<db.Tables()[a[1]].Lines(); j++)
			{
				if(type == 1)
				{
					if(db.Tables()[a[0]].Maintable()[i][ind1] == db.Tables()[a[1]].Maintable()[j][ind2])
					{
						unite_table[i][j] = 1;
						s++;
					}
					else unite_table[i][j] = 0;
				}
				else if(type == 2)
				{
					if(db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][ind1]].hash == db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[j][ind2]].hash)
					{
						char* buf1 = (char*)malloc((db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][ind1]].length + 1) * sizeof(char));
						char* buf2 = (char*)malloc((db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[j][ind2]].length + 1) * sizeof(char));
						memcpy(buf1, db.Tables()[a[0]].TextData() + db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][ind1]].offset, db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][ind1]].length);
						buf1[db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][ind1]].length] = '\0';
						memcpy(buf2, db.Tables()[a[1]].TextData() + db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[j][ind2]].offset, db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[j][ind2]].length);
						buf2[db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[j][ind2]].length] = '\0';
						
						if(strcmp(buf1, buf2) == 0)
						{
							unite_table[i][j] = 1;
							s++;
						}
						else unite_table[i][j] = 0;
					}
					else unite_table[i][j] = 0;
				}
			}
		}
		
		//������������ ������� ����������� �� ��������
		if(c.ct != 0 && c.dt != 0)
		{
			int tn = -1, tc = -1, flag = 0; //table number, table coloumn
			for(int i = 0; i< db.Tables()[a[0]].Coloumns(); i++)
				if(strcmp(db.Tables()[a[0]].Headers()[i].title, c.title) == 0)
				{
					tn = a[0];
					tc = i;
					flag = 1;
					break;
				}
			if(flag == 0)
			for(int i = 0; i< db.Tables()[a[1]].Coloumns(); i++)
				if(strcmp(db.Tables()[a[1]].Headers()[i].title, c.title) == 0)
				{
					tn = a[1];
					tc = i;
					flag = 1;
				}
			

			if(tn == 0)
			{
				for(int i = 0; i < db.Tables()[a[0]].Lines(); i++)
				{
					char* buf1 = (char*)malloc(sizeof(char) * L);
					char* buf2 = (char*)malloc(sizeof(char) * L);
					if(c.dt == (datatype)1)
						itoa(db.Tables()[a[0]].Maintable()[i][tc], buf1, P);
					else
					{
						memcpy(buf1, db.Tables()[a[0]].TextData() + db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][tc]].offset, db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][tc]].length);
						buf1[db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][tc]].length] = '\0';
					}
					memcpy(buf2, c.data, strlen(c.data) + 1);
					if(Operation1(c.ct, buf1, buf2, c.dt) == 0 )
					{
						for(int j = 0; j < db.Tables()[a[1]].Lines(); j++)
							if(unite_table[i][j] == 1)
							{
								unite_table[i][j] = 0;
								s--;
							}
					}
				}
			}
			else
			{
				for(int i = 0; i < db.Tables()[a[1]].Lines(); i++)
				{
					char* buf1 = (char*)malloc(sizeof(char) * L);
					char* buf2 = (char*)malloc(sizeof(char) * L);
					if(c.dt == (datatype)1)
						itoa(db.Tables()[a[1]].Maintable()[i][tc], buf1, P);
					else
					{
						memcpy(buf1, db.Tables()[a[1]].TextData() + db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[i][tc]].offset, db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[i][tc]].length);
						buf1[db.Tables()[a[0]].Posts()[db.Tables()[a[1]].Maintable()[i][tc]].length] = '\0';
					}
					memcpy(buf2, c.data, strlen(c.data) + 1);
					if(Operation1(c.ct, buf1, buf2, c.dt) == 1 )
					{
						for(int j = 0; j < db.Tables()[a[0]].Lines(); j++)
							if(unite_table[j][i] == 1)
							{
								unite_table[j][i] = 0;
								s--;
							}
					}
				}
			}
		}

		//������ ������� ������

		showTitle* showtitles = (showTitle*)malloc(sizeof(showTitle) * n_titles);
		
		for(int i = 0; i < n_titles; i++)
			showtitles[i].title.title = (char*)malloc(sizeof(char) * L);

		if(all)
		{
			int i = 0;
			for(int j = 0; j<db.Tables()[a[0]].Coloumns(); j++)
			{
				memcpy(showtitles[i].title.title, db.Tables()[a[0]].Headers()[j].title, strlen(db.Tables()[a[0]].Headers()[j].title) + 1);
				showtitles[i].title.type = db.Tables()[a[0]].Headers()[j].type;
				showtitles[i].ntable = 0;
				showtitles[i].ncolounm = j;
				i++;
			}
			for(int j = 0; j<db.Tables()[a[1]].Coloumns(); j++)
			{
				memcpy(showtitles[i].title.title, db.Tables()[a[1]].Headers()[j].title, strlen(db.Tables()[a[1]].Headers()[j].title) + 1);
				showtitles[i].title.type = db.Tables()[a[1]].Headers()[j].type;
				showtitles[i].ntable = 1;
				showtitles[i].ncolounm = j;
				i++;
			}
		}
		else
		{
			int test = 0, i =0;
			for(i = 0; i < n_titles; i++)
			{
				for(int j = 0; j<db.Tables()[a[0]].Coloumns(); j++)
				{
					if(strcmp(titles[i], db.Tables()[a[0]].Headers()[j].title) == 0)
					{
						showtitles[i].title.type = db.Tables()[a[0]].Headers()[j].type;
						memcpy(showtitles[i].title.title, db.Tables()[a[0]].Headers()[j].title, strlen(db.Tables()[a[0]].Headers()[j].title) + 1);
						showtitles[i].ntable = 0;
						showtitles[i].ncolounm = j;
						i++;
						test++;
						j = 0;
					}
				}
				for(int j = 0; j<db.Tables()[a[1]].Coloumns(); j++)
				{
					if(strcmp(titles[i], db.Tables()[a[1]].Headers()[j].title) == 0)
					{
						showtitles[i].title.type = db.Tables()[a[1]].Headers()[j].type;
						memcpy(showtitles[i].title.title, db.Tables()[a[1]].Headers()[j].title, strlen(db.Tables()[a[1]].Headers()[j].title) + 1);
						showtitles[i].ntable = 1;
						showtitles[i].ncolounm = j;
						i++;
						test++;
					}
				}
			}
		}

		mainarr_off = (int**)malloc(sizeof(int*) * (s + 1));
		mainarr_l = (int**)malloc(sizeof(int*) * (s + 1));
		for(int i = 0; i < s + 1; i++)
		{
			mainarr_off[i] = (int*)malloc(sizeof(int) * n_titles);
			mainarr_l[i] = (int*)malloc(sizeof(int) * n_titles);
		}

		for(int i = 0; i < n_titles; i++)
		{
			mainarr_off[0][i] = strlen(textline);
			mainarr_l[0][i] = strlen(showtitles[i].title.title);
			strcat(textline, showtitles[i].title.title);
		}
		
		for(int i = 0, k = 1; i < db.Tables()[a[0]].Lines(); i++)
			for(int j = 0; j < db.Tables()[a[1]].Lines(); j++)
				if(unite_table[i][j] == 1)
				{
					for(int l = 0; l < n_titles; l++)
					{
						if(showtitles[l].ntable == a[0])
						{
							if(showtitles[l].title.type == (datatype)1)
							{
								itoa(db.Tables()[a[0]].Maintable()[i][showtitles[l].ncolounm], buf, P);

								mainarr_off[k][l] = strlen(textline);
								mainarr_l[k][l] = strlen(buf);
								strcat(textline, buf);
							}
							if(showtitles[l].title.type == (datatype)2)
							{
								memcpy(buf, db.Tables()[a[0]].TextData() + db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][showtitles[l].ncolounm]].offset, db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][showtitles[l].ncolounm]].length);
								buf[db.Tables()[a[0]].Posts()[db.Tables()[a[0]].Maintable()[i][showtitles[l].ncolounm]].length] = '\0';

								mainarr_off[k][l] = strlen(textline);
								mainarr_l[k][l] = strlen(buf);
								strcat(textline, buf);
							}
						}
						if(showtitles[l].ntable == a[1])
						{
							if(showtitles[l].title.type == (datatype)1)
							{
								itoa(db.Tables()[a[1]].Maintable()[j][showtitles[l].ncolounm], buf, P);

								mainarr_off[k][l] = strlen(textline);
								mainarr_l[k][l] = strlen(buf);
								strcat(textline, buf);
							}
							if(showtitles[l].title.type == (datatype)2)
							{
								memcpy(buf, db.Tables()[a[1]].TextData() + db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[j][showtitles[l].ncolounm]].offset, db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[j][showtitles[l].ncolounm]].length);
								buf[db.Tables()[a[1]].Posts()[db.Tables()[a[1]].Maintable()[j][showtitles[l].ncolounm]].length] = '\0';

								mainarr_off[k][l] = strlen(textline);
								mainarr_l[k][l] = strlen(buf);
								strcat(textline, buf);
							}
						}
					}
					k++;
				}
	a_lines = s + 1;
	a_coloumns = n_titles;
		
	}
	else if(N == 1)
	{
		int n = -1; // ����� �������
		for(int i = 0; i < db.Count(); i++)
			if(strcmp(db.Tables()[i].Name(), tab[0].tablename) == 0)
			{
				if(all)
					n_titles = db.Tables()[i].Coloumns();
				n = i;
				break;
			}
		if(n == -1)
		{
			cout<<"Error"<<endl;
			return;
		}
		showTitle* showtitles = (showTitle*)malloc(sizeof(showTitle) * n_titles);
		if(all)
			for(int i = 0; i < n_titles; i++)
			{
				showtitles[i].ntable = n;
				showtitles[i].ncolounm = i;
				showtitles[i].title.type = db.Tables()[n].Headers()[i].type;
				showtitles[i].title.title = (char*)malloc(sizeof(char) * L);
				memcpy(showtitles[i].title.title, db.Tables()[n].Headers()[i].title, strlen(db.Tables()[n].Headers()[i].title) + 1);
			}
		else 
			for(int i = 0; i < n_titles; i++)
			{
				for(int j = 0; j < db.Tables()[n].Coloumns(); j++)
					if(strcmp(db.Tables()[n].Headers()[j].title, titles[i]) == 0)
					{
						showtitles[i].ntable = n;
						showtitles[i].ncolounm = j;
						showtitles[i].title.type = db.Tables()[n].Headers()[j].type;
						showtitles[i].title.title = (char*)malloc(sizeof(char) * L);
						memcpy(showtitles[i].title.title, db.Tables()[n].Headers()[j].title, strlen(db.Tables()[n].Headers()[j].title) + 1);
						break;
					}
			}

		int k = 0;
		int* arrs = (int*)malloc(sizeof(int) * db.Tables()[n].Lines());
		
		if(c.ct != 0 && c.dt != 0)			
		{
			int tc = -1; //table number, table coloumn
			for(int i = 0; i< db.Tables()[n].Coloumns(); i++)
				if(strcmp(db.Tables()[n].Headers()[i].title, c.title) == 0)
				{
					tc = i;
					break;
				}
			for(int i = 0; i < db.Tables()[n].Lines(); i++)
			{
				char* buf1 = (char*)malloc(sizeof(char) * L);
				char* buf2 = (char*)malloc(sizeof(char) * L);
				if(c.dt == (datatype)1)
					itoa(db.Tables()[n].Maintable()[i][tc], buf1, P);
				else
				{
					memcpy(buf1, db.Tables()[n].TextData() + db.Tables()[n].Posts()[db.Tables()[n].Maintable()[i][tc]].offset, db.Tables()[n].Posts()[db.Tables()[n].Maintable()[i][tc]].length);
					buf1[db.Tables()[n].Posts()[db.Tables()[n].Maintable()[i][tc]].length] = '\0';
				}
				memcpy(buf2, c.data, strlen(c.data) + 1);
				if(Operation1(c.ct, buf1, buf2, c.dt) == 1 )
				{
					arrs[i] = 1;
					k++;
				}
				else arrs[i] = 0;
				free(buf1);
				free(buf2);
			}
		}
		else 
		{	
			for(int i = 0; i< db.Tables()[n].Lines(); i++)
				arrs[i] = 1;
			k = db.Tables()[n].Lines();
		}
	
		//������ ��������� �������

		mainarr_off = (int**)malloc(sizeof(int*) * (k + 1));
		mainarr_l = (int**)malloc(sizeof(int*) * (k + 1));
		for(int i = 0; i< k + 1; i++)
		{
			mainarr_off[i] = (int*)malloc(sizeof(int) * n_titles);
			mainarr_l[i] = (int*)malloc(sizeof(int) * n_titles);
		}

		for(int i = 0; i < n_titles; i++)
		{
			mainarr_off[0][i] = strlen(textline);
			mainarr_l[0][i] = strlen(showtitles[i].title.title);
			strcat(textline, showtitles[i].title.title);
		}


		
		int l = 1;

		for(int i = 0; i < db.Tables()[n].Lines(); i++)
			if(arrs[i] == 1)
			{
				for(int j = 0; j < n_titles; j++)
				{

					if(showtitles[j].title.type == (datatype)1)
					{
						itoa(db.Tables()[n].Maintable()[i][showtitles[j].ncolounm], buf, P);
						mainarr_off[l][j] = strlen(textline);
						mainarr_l[l][j] = strlen(buf);
						strcat(textline, buf);
					}
					if(showtitles[j].title.type == (datatype)2)
					{
						memcpy(buf, db.Tables()[n].TextData() + db.Tables()[n].Posts()[db.Tables()[n].Maintable()[i][showtitles[j].ncolounm]].offset, db.Tables()[n].Posts()[db.Tables()[n].Maintable()[i][showtitles[j].ncolounm]].length);
						buf[db.Tables()[n].Posts()[db.Tables()[n].Maintable()[i][showtitles[j].ncolounm]].length] = '\0';

						mainarr_off[l][j] = strlen(textline);
						mainarr_l[l][j] = strlen(buf);
						strcat(textline, buf);
					}
				}
				l++;
			}
			a_lines = l;
			a_coloumns = n_titles;
	}

	
	int* arr = (int*)malloc(sizeof(int) * a_coloumns);
	for(int j = 0; j < a_coloumns; j++)
	{
		arr[j] = 0;
		for(int i = 0; i < a_lines; i++)
			if(mainarr_l[i][j] > arr[j])
				arr[j] = mainarr_l[i][j];
	}

	//�������!
	cout<<"Results of the SEL request N"<<y<<endl<<endl;
	cout.setf(ios::left);

	for(int i = 0; i < a_lines; i++)
	{
		for(int j = 0; j < a_coloumns; j++)
		{
			memcpy(buf, textline + mainarr_off[i][j], mainarr_l[i][j]);
			buf[mainarr_l[i][j]] = '\0';
			cout.width(arr[j]);

			cout<<buf<<"   ";
		}
		cout<<endl;
	}
	cout<<endl<<endl<<endl<<endl<<endl;
	
	free(arr);
	free(buf);
	for(int i = 0; i < a_lines; i++)
	{
		free(mainarr_l[i]);
		free(mainarr_off[i]);
	}
	free(mainarr_l);
	free(mainarr_off);
	
	
	int ghd = 9;
}

//��������
void cmd_crt(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');
	
	int z = 0;
	for(int i = 0; i < strlen(line); i++)
		if(line[i] == ',')
			z++;
	if(z == 0)
		throw new exception;

	Table t = Table(name, 0, z + 1);

	oTitle* titles = MakeLine(line, z + 1);
	t.SetOtitles(titles);
	
	db.AddTable(t);
}

//��������
void cmd_ins(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');

	int k = -1;
	for (int i = 0; i<db.Count(); i++)
	{
		if(strcmp(db.Tables()[i].Name(), name) == 0)
			k = i;
	}
	if(k == -1)
		throw new exception;

	char datatext[L];
	datatext[0] = '\0';
	int* data = (int*)malloc(sizeof(int)*db.Tables()[k].Coloumns());
	post* postdata = (post*)malloc(0);
	int posts = 0;
	postdata = MakeOneLine(line, db.Tables()[k].Headers(), db.Tables()[k].Coloumns(), datatext, data, postdata, &posts);
	db.Tables()[k].AddLine(data, postdata, datatext, posts);
}

//��������
void cmd_upd(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');
	char* cond = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, cond, ' ');

	Condition c;

	c.title = (char*) malloc(L * sizeof(char));
	c.ct = (condtype) 0;
	c.dt = (datatype) 0;
	c.data = (char*) malloc(L * sizeof(char));

	GetCondition(cond, &c);

	char* title = (char*) malloc(L * sizeof(char));
	char* data = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, title, '=');

	if(strchr(line, '\'') != 0x00)
	{
		memcpy(data, line + sizeof('\''), strlen(line) - 2 * sizeof('\''));
		data[strlen(line) - 2 * sizeof('\'')] = '\0';
	}
	else memcpy(data, line, strlen(line) + sizeof('\0'));

	int k = -1;
	for (int i = 0; i<db.Count(); i++)
	{
		if(strcmp(db.Tables()[i].Name(), name) == 0)
			k = i;
	}
	if(k == -1)
		throw new exception;

	db.Tables()[k].ChangeLine(c, title, data);

	int y = 0;
}

//��������
void cmd_det(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');
	db.DelTable(line);
}

//��������
void cmd_del(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');

	Condition c;

	c.title = (char*) malloc(L * sizeof(char));
	c.ct = (condtype) 0;
	c.dt = (datatype) 0;
	c.data = (char*) malloc(L * sizeof(char));

	GetCondition(line, &c);

	int k = -1;
	for (int i = 0; i<db.Count(); i++)
	{
		if(strcmp(db.Tables()[i].Name(), name) == 0)
			k = i;
	}
	if(k == -1)
		throw new exception;

	db.Tables()[k].DelLine(c);
}

//��������
//�� ������ � ���, ��� ����� - � �������!
//y - ���-�� SELECT'��
void cmd_sel(char* line, int* y)
{
	sel_sort sort;
	
	char* tables = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, tables, ' ');
	
	char* vu = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, vu, ' ');

	if(strcmp(line, vu) == 0)
	{
		sort.title = (char*)malloc(sizeof('\0'));
		sort.title[0] = '\0';
		sort.up_down = 0;
	}
	else
	{
		if(line[strlen(line) - 1] == '+')
			sort.up_down = +1;
		else 
		{
			if(line[strlen(line) - 1] == '-')
				sort.up_down = -1;
			else throw new exception;
		}
		sort.title = (char*)malloc(strlen(line) * sizeof(char));
		memcpy(sort.title, line, strlen(line) - 1);
		sort.title[strlen(line) - 1] = '\0';
	}

	char*viv = (char*)malloc(strlen(vu) * sizeof(char));
	GetTwoLines(vu, viv, ';');

	Condition c;
	c.data = (char*)malloc(strlen(vu) * sizeof(char));
	c.title = (char*)malloc(strlen(vu) * sizeof(char));

	if(strcmp(viv, vu) == 0)
	{
		c.ct = (condtype)0;
		c.dt = (datatype)0;
		c.data[0] = '\0';
		c.title[0] = '\0';
	}
	else GetCondition(vu, &c);

	int z = 0;
	for(int i = 0; i < strlen(viv); i++)
	{
		if(viv[i] == ',')
			z++;
	}
	if(z == 0)
	{
		if(viv[0] == '*')
			z = -1;
	}
	else z++;

	char** titles = (char**)malloc(sizeof(char*) * abs(z));

	if(z == -1)
	{
		titles[0] = (char*)malloc(sizeof(char) * 2);
		titles[0][0] = '*';
		titles[0][1] = '\0';
	}
	else
		for(int i = 0; i < z; i++)
			titles[i] = (char*)malloc(sizeof(char) * strlen(viv));
	GetVivTitles(viv, z, titles);
	
	int N_t = 0;

	sel_tab* tab;

	if(strchr(tables, ';') == 0x00)
	{
		N_t = 1;
		tab = (sel_tab*)malloc(sizeof(sel_tab) * N_t);
		tab[0].tablename = (char*)malloc(sizeof(char) * strlen(tables) + sizeof('\0'));
		memcpy(tab[0].tablename, tables,  strlen(tables) + sizeof('\0'));
		tab[0].title = (char*)malloc(sizeof('\0'));
		tab[0].title[0] = '\0';
	}
	else 
	{
		N_t = 2;
		tab = (sel_tab*)malloc(sizeof(sel_tab) * N_t);
		char* tables1 = (char*)malloc(sizeof(char) * strlen(tables));
		char* lefttable = (char*)malloc(sizeof(char) * strlen(tables));
		char* lefttitle = (char*)malloc(sizeof(char) * strlen(tables));
		GetTwoLines(tables, tables1, ';');
		GetTwoLines(tables1, lefttable, ',');
		GetTwoLines(tables, lefttitle, '=');

		//tables = ������ title
		//tables1 = ������ �������� �������
		//lefttable = ����� �������� �������
		//lefttitle = ����� title

		tab[0].tablename = (char*)malloc(sizeof(char) * strlen(lefttable) + sizeof('\0'));
		tab[0].title = (char*)malloc(sizeof(char) * strlen(lefttitle) + sizeof('\0'));
		tab[1].tablename = (char*)malloc(sizeof(char) * strlen(tables1) + sizeof('\0'));
		tab[1].title = (char*)malloc(sizeof(char) * strlen(tables) + sizeof('\0'));
		
		memcpy(tab[0].tablename, lefttable, sizeof(char) * strlen(lefttable) + sizeof('\0'));
		memcpy(tab[0].title, lefttitle, sizeof(char) * strlen(lefttitle) + sizeof('\0'));
		memcpy(tab[1].tablename, tables1, sizeof(char) * strlen(tables1) + sizeof('\0'));
		memcpy(tab[1].title, tables, sizeof(char) * strlen(tables) + sizeof('\0'));

	}
	ShowSelected(*y, N_t, tab, titles, z, c, sort);
	(*y)++;
}

void command2(char* line, int* y)
{
	if(line[strlen(line) - 1] == '\n')
		line[strlen(line) - 1] = '\0';
	if(strchr(line, ' ') - line !=3)
		throw new exception;
	const int d = 4;
	char cmdname[d];
	memcpy(cmdname, line, d - 1);
	cmdname[d-1] = '\0';
	
	char cmddata[L];
	memcpy(cmddata, line + d, strlen(line) - d);
	cmddata[strlen(line) - d ] = '\0';

	if(cmdname[0] == 'C' && cmdname[1] == 'R'&& cmdname[2] == 'T')
		cmd_crt(cmddata);
	else 
		if(cmdname[0] == 'I' && cmdname[1] == 'N'&& cmdname[2] == 'S')
			cmd_ins(cmddata);
		else
			if(cmdname[0] == 'U' && cmdname[1] == 'P'&& cmdname[2] == 'D')
				cmd_upd(cmddata);
			else
				if(cmdname[0] == 'D' && cmdname[1] == 'E'&& cmdname[2] == 'T')
					cmd_det(cmddata);
				else
					if(cmdname[0] == 'D' && cmdname[1] == 'E'&& cmdname[2] == 'L')
						cmd_del(cmddata);
					else
						if(cmdname[0] == 'S' && cmdname[1] == 'E'&& cmdname[2] == 'L')
							cmd_sel(cmddata, y);
						else throw new exception;

}
void command(char* filename)
{
	char buff[L];

	try
	{
		FILE *f;
		int y = 1; //���-�� SELECT'��
		f = fopen(filename, "r");
		while(!feof(f))
		{
			fgets(buff, L, f);
			command2(buff, &y);
		}
	}
	catch (exception e)
	{
		printf("Error reading commands\n");
	}
}

int main(int argc, char* argv[])
{
	input(argv[1]);
	command(argv[2]);
	output(argv[3]);
	cout<<"The program has fineshed. The results of all SEL-requests displayed. The DB recorded in file \""<<argv[3]<<"\". Thank you!"<<endl<<"Press enter to close the window"<<endl;
	getchar();
    return 0;
}

#include "StdAfx.h"

const int L = 15000;
const int LS = 100000000;

//��������� ������� ���������
const int P = 10;

//��� ����
const int H = 10;
const int MULT = 31;

enum datatype
{
	integer = 1,
	line = 2
};

enum condtype
{
	Equally = 1,
	NotEqually = 2,
	More = 3,
	MoreOrEqually  = 4,
	Less = 5,
	LessOrEqually = 6
};

struct Condition
{
	char* title;
	condtype ct;
	datatype dt;
	char* data;
};

struct oTitle
{
	char* title;
	datatype type;
};

struct showTitle
{
	int ntable;
	int ncolounm;
	oTitle title;
};

struct post
{
	unsigned int offset;
	unsigned int length;
	unsigned int hash;
	unsigned int deleted;
};

struct sel_tab
{
	char* tablename;
	char* title;
};

struct sel_sort
{
	char* title;
	int up_down;
};



class Table
{
	private:
		char *name;
		unsigned int lines, coloumns, real_lines;
		oTitle*	headers;
		int** maintable;
		post* posts;
		char* textdata;
		unsigned int n_posts, n_textdata, n_maxtextdata, n_maxposts;
       	
	public:
		Table(char* name, unsigned int lines, unsigned int coloumns);
		void SetOtitles(oTitle* headers);
		void AddLine(int* line, post* postdata, char* textdata, int posts);
		int LengthOfTextData();
		char* Name();
		int Lines();
		int Coloumns();
		oTitle*	Headers();
		int** Maintable();
		post* Posts();
		char* TextData();
		void ChangeLine(Condition c, char* title, char* data);
		int myhash(char* line);
		void DelLine(Condition c);
		
};

class DataBase
{
	private:
		char* name;
		int count, countmax;
		Table* tables;

	public:
		DataBase();
		DataBase(char* name);
		char* Name();
		int Count();
		Table* Tables();
		void AddTable(Table t);
		void DelTable(char* name);
		
};